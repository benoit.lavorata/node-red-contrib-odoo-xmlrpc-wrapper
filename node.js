module.exports = function (RED) {

    var handle_error = function (err, node) {
        node.log(err.body);
        node.status({
            fill: "red",
            shape: "dot",
            text: err.message
        });
        node.error(err.message);
    };

    function MyNode(config) {
        const node = this;
        RED.nodes.createNode(node, config);
        const Odoo = require('./Odoo.js');
        const logic = require('./logic.js');
        process.setMaxListeners(0);

        node.on('input', function (msg) {
            node.status({
                fill: "blue",
                shape: "dot",
                text: `Running...`
            });

            logFunction = function (_msg) {
                node.log(_msg);
                node.status({
                    fill: "blue",
                    shape: "dot",
                    text: `${_msg}...`
                });

                if (msg.payload.forwardLogs) {
                    node.send({
                        topic: 'log',
                        payload: _msg,
                        original: msg
                    });
                }
            }

            msg['_original'] = msg.payload;
            if (!node._odooInstance) {
                node._odooInstance = new Odoo(msg.payload);
            }

            logic
                .onMessage(msg, node._odooInstance, logFunction)
                .then(data => {
                    node.status({
                        fill: "green",
                        shape: "dot",
                        text: `Success !`
                    });
                    msg.payload = data;
                    node.send(msg);
                }).catch(err => {
                    node.error(err);
                    handle_error(err, node);
                    msg.payload = false;
                    node.send(msg);
                });
        });
    }
    RED.nodes.registerType("odoo_xmlrpc_wrapper", MyNode);
};