const onMessage = async (msg, instance, logFunction = false) => {
        if (logFunction) {
                instance.log = logFunction;
        }
        return instance.call(msg.payload.method, msg.payload.args || []);
}
module.exports = {
        onMessage: onMessage
}