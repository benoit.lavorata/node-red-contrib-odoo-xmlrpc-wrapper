const Odoo = require('./Odoo.js');

var msg = {};
var inParams = [];
inParams.push([
    ['is_company', '=', true],
    ['customer', '=', true]
]);
inParams.push(['name', 'country_id', 'comment']); //fields
inParams.push(0); //offset
inParams.push(5); //limit
var params = [];
params.push(inParams);

msg.payload = {
    odooConfig: {
        url: "YOUR URL",
        port: 443,
        db: "YOUR DB",
        username: "admin",
        password: "admin",
    }
};

let instance = new Odoo(msg.payload);
async function getReports(domain = []) {
    console.log('getReports');
    msg.payload = {
        method: 'execute_kw',
        args: [
            'account.aspone',
            'search_read', [
                [
                    domain, // ['to_push', '=', true],
                    [],
                    0,
                    5
                ]
            ]
        ]
    };
    return instance.call(msg.payload.method, msg.payload.args);
}

async function getReportLines(aspone_line_ids = [-1]) {
    console.log('getReportLines');
    msg.payload = {
        method: 'execute_kw',
        args: [
            'account.aspone.line',
            'search_read', [
                [
                    [
                        ['id', '=', aspone_line_ids],
                    ],
                    [],
                    0,
                    500
                ]
            ]
        ]
    };

    return instance.call(msg.payload.method, msg.payload.args);
}

async function start() {
    const reports = await getReports();
    for (let i = 0; i < reports.length; i++) {
        const report = reports[i];
        const lines = await getReportLines(report.aspone_line_ids);
        report['_lines'] = lines;
    }
}

start().then(r => console.log).catch(e => console.error)