const odooLib = require('odoo-xmlrpc');

class Odoo {
    constructor({
        tempFilePath = './data/temp/odoosh',
        odooConfig = {
            url: false,
            port: 80,
            db: false,
            username: false,
            password: false,
        }
    }) {
        const s = this;
        s.tempFilePath = tempFilePath;
        s.odooConfig = odooConfig;

        if (!s.odooConfig.url ||
            !s.odooConfig.db ||
            !s.odooConfig.username ||
            !s.odooConfig.password) {
            throw new Error("Wrong credentials for Odoo")
        }
        //s.log(`Odoo class instantiated`);
    }

    sleep(timer, comment = 'node') {
        const s = this;
        s.log(`    sleep for: ${timer} ms (${comment})`);
        return new Promise(res => setTimeout(res, timer))
    }

    log(msg) {
        console.log(msg)
    }

    async autoConnect(retry = 0) {
        const s = this;
        //s.log(`autoConnect: ${retry}`);

        if (retry > 3) {
            s.isConnecting = false;
            s.isConnected = false;
            s.log(`Failed to autoConnect at ${retry}`);
            return false;
        }

        if (s.isConnecting) {
            await s.sleep(1000);
            return s.autoConnect(retry + 1);
        }

        if (s.isConnected) {
            s.isConnecting = false;
            s.log(`is connected`);
            return true;
        }

        s.isConnected = false;
        s.isConnecting = true;

        s.odoo = new odooLib(s.odooConfig);

        s.log(`Attempt connection at ${retry}`);
        return new Promise((res, rej) => {
            s.odoo.connect(function (err) {
                if (err) {
                    s.isConnected = false;
                    s.isConnecting = false;
                    s.log(`Failed to autoConnect at ${retry}`);
                    rej(err);
                    return false;
                }

                s.log(`is connected`);
                s.isConnected = true;
                s.isConnecting = false;
                res(true);
                return true;
            });
        });
    }

    async call(method, args) {
        const s = this;
        //s.log(`call: ${method}`);

        await s.autoConnect();

        return new Promise((res, rej) => {
            (s.odoo[method])(...args, function (err, value) {
                if (err) {
                    rej(err);
                    return false;
                }
                res(value);
                return true;
            })
        });
    }

    async start(opts) {
        const s = this;
        s.log(`start `);
    }

    init() {
        const s = this;
        s.log(`init `);
        s.start().catch(e => console.error(e))
    }
}

module.exports = Odoo;